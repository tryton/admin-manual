==========================================================
Tryton Administrators Manual
==========================================================

Target audience:
Persons administering Tryton (within Tryton)

This manual describes how to configure the various aspects of Tryton
within Tryton, e.g.:

- users, roles and permissions,
- customizing templates,
- set-up and configuration of the most important modules,
- …

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
